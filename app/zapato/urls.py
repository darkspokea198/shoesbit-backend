from django.conf.urls import url
from .views import *

urlpatterns = [
    url(r"^list-modelshoes/", ListModelShoesAPIView.as_view()),
    url(r"^list-shoes/", ListShoesAPIView.as_view()),
]



