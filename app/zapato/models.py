from django.db import models

# Create your models here.

class ModelShoes(models.Model):
    
    name = models.CharField(max_length=100, blank=True, null=True)
    description = models.CharField(max_length=200, blank=True, null=True)

    def __str__(self):
        return '{0}'.format(self.name)


class Shoes(models.Model):
    
    name = models.CharField(max_length=100, blank=True, null=True)
    price = models.DecimalField(max_digits=6, decimal_places=2, null = True, blank=True)
    description = models.CharField(max_length=200, blank=True, null=True)
    image = models.ImageField(upload_to="media/",null=True,blank=True)
    model = models.OneToOneField(ModelShoes, verbose_name="Modelo", on_delete=models.CASCADE,null=True,blank=True)


    def __str__(self):
        return '{0}'.format(self.name)
