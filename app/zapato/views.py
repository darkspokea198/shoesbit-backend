from django.shortcuts import render
from .serializers import *
from rest_framework import generics
from rest_framework.views import APIView 
# Create your views here.

class ListModelShoesAPIView(generics.ListAPIView):
    serializer_class = ModelShoesSerializer

    def get_queryset(self):
        return ModelShoes.objects.all()

class ListShoesAPIView(generics.ListAPIView):
    serializer_class = ShoesSeralizers

    def get_queryset(self):
        return Shoes.objects.all()
