from rest_framework import serializers
from .models import *
from datetime import timedelta

class ModelShoesSerializer(serializers.ModelSerializer):
    class Meta:
        model = ModelShoes
        fields = ('id','name','description')

class ShoesSeralizers(serializers.ModelSerializer):
    modelShoes = ModelShoesSerializer(many=True, read_only=True)

    class Meta:
        model = Shoes
        fields = ('id','name','price','description','image','modelShoes')
