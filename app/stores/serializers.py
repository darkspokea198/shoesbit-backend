from rest_framework import serializers
from .models import Stores
from django.conf import settings
from django.core.files.storage import default_storage

class StoresSerializer(serializers.ModelSerializer):
    class Meta:
        model = Stores
        fields = ('id', 'name')

class ListStoresSerializer(serializers.ModelSerializer):
    class Meta:
        model = Stores
        fields = ('id','longitude','latitude','name')