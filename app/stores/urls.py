from django.conf.urls import url
from .views import *

urlpatterns = [
    url(r'^list-stores/', ListStoresAPIView.as_view()),
]

