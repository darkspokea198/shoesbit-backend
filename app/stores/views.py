from rest_framework import viewsets, generics
from .serializers import *
from .models import Stores
# Create your views here.

class ListStoresAPIView(generics.ListAPIView):
    serializer_class = ListStoresSerializer
    def get_queryset(self):
        return Stores.objects.all()
