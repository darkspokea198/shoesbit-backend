from django.db import models

# Create your models here.

class Stores(models.Model):

    latitude = models.FloatField(blank=True, null=True, verbose_name='Latitud')
    longitude = models.FloatField(blank=True, null=True,verbose_name='Longitud')
    name = models.CharField(max_length=200, blank=True, null=True, verbose_name='Nombre')

    class Meta:
        verbose_name = "Tienda"
        verbose_name_plural = "Tiendas"

    def __str__(self):
        return '{0}'.format(self.name)
